<h1>Bem vindo ao Front-End do projeto Conig Plataforma de Aprendizagem</h1>

Para acessar o nosso site basta clicar [aqui](https:\\app.conig.info) ou digitar no seu navegador <strong>app.conig.info</strong>!
Alguns usuários de teste:

Email: alunoteste@gmail.com <br>
Senha: conig123

Email: alunoteste2@gmail.com <br>
Senha: conig123

**Os usuários de testes vão ser resetados periodicamente**



# Como instalar e usar?

Para instalar é simples, siga os passos:

1. Baixe e instale o [Node.js](https://nodejs.org/en/) na sua máquina (A versão recomendade);
1. Abra seu prompt de comandos ou terminal;
1. Navegue até o diretório desejado;
1. Clone este repositório;
1. Navegue através do prompt ou terminal para dentro deste repositório;
1. Digite o comando "***npm install***";
1. Após terminar o processo, digite o comando "***npm start***";

#### Pronto o WebApp está rodando.

#### NÃO ESQUEÇA PARA O WEBAPP RODAR CORRETAMENTE É NECESSÁRIO RODAR O BACKEND SIMULTANEAMENTE!

# Onde encontro o Backend?

Você consegue encontrar o Backend [clicando aqui](https://gitlab.com/projeto-de-desenvolvimento/controle-de-estoque-vk/conig-api).
