import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router'
import { Redirect, Link } from 'react-router-dom'
import ModalComplete from '../../components/modalComplete'
import QuestionCard from '../../components/questionCard'
import QuestionMultiple from '../../components/questionMultipleCard'
import QuestionString from '../../components/questionStringCard'
import SideNav from '../../components/sideNav'
import SiteHeader from '../../components/siteHeader'
import api from '../../services/api'
import './styles.css'

function Exercise() {
	const [module, setModule] = useState({})
	const [redirect, setRedirect] = useState(false)
	const [exercise, setExercise] = useState({})
	const [showModal, setShowModal] = useState(false)
	const [questions, setQuestions] = useState([])
	const [links, setLinks] = useState([])
	const { id, module_id } = useParams()

	useEffect(() => {
		async function getQuestions() {
			try {
				const { data } = await api.get(`/modules/${module_id}`)
				const exercise = await api.get(`/exercises/${id}`)
				setModule(data.module)
				setExercise(exercise.data)
				setQuestions(exercise.data.questions)
				setLinks(data.module.classes)
			} catch (err) {
				console.log(err)
			}
		}
		getQuestions()
	}, [id])
	async function handleSubmit(e) {
		e.preventDefault()
		const response = await api
			.post('studentExercise', {
				exercise_id: id
			})
			.then(function(response) {
				console.log(response)
				setRedirect(true)
			})
	}

	async function completeCourse() {
		await api.post('/studentExercise', {
			exercise_id: id
		})
		setShowModal(true)
	}
	return (
		<div>
			{redirect && <Redirect push to={exercise.next_exercise} />}
			<SiteHeader />
			{exercise.next_exercise === 'last' && (
				<ModalComplete show={showModal} module_id={module_id} />
			)}
			<main className="main-container">
				<SideNav
					data={links}
					is_exercise
					module_id={module_id}
					module_name={module.name}
					tutorial_id={id}
				/>

				<section className="content">
					<span className="welcome">Bem vindo ao exercicios de {module.name}</span>

					<div className="instructions">
						<span className="instructions__title">
							Aqui estão algumas instruções para que consiga fazer os exercicios sem problemas.
						</span>
						<ul className="instructions__list">
							<li>Cada questão acertada vale 50 XP</li>
							<li>Cada questão errada vale 25 XP</li>
						</ul>
					</div>

					<div className="exercises">
						{questions.map(question => (
							<>
								{question.type === 'VF' ? (
									<QuestionCard
										statement={question.statement}
										question={question.question}
										response={question.response}
										response_feedback={question.response_feedback}
										difficulty={question.difficulty}
										question_id={question.id}
										student_response={
											question.studentResponse[0] ? question.studentResponse[0].response : ''
										}
									/>
								) : question.type === 'MULTIPLE' ? (
									<QuestionMultiple
										statement={question.statement}
										question={question.question}
										response={question.response}
										response_feedback={question.response_feedback}
										question_id={question.id}
										difficulty={question.difficulty}
										student_response={
											question.studentResponse[0] ? question.studentResponse[0].response : ''
										}
										alternative_1={question.alternative_1}
										alternative_2={question.alternative_2}
										alternative_3={question.alternative_3}
										alternative_4={question.alternative_4}
									/>
								) : (
									<QuestionString
										statement={question.statement}
										question={question.question}
										response={question.response}
										difficulty={question.difficulty}
										response_feedback={question.response_feedback}
										question_id={question.id}
										student_response={
											question.studentResponse[0] ? question.studentResponse[0].response : ''
										}
									/>
								)}
							</>
						))}
						{exercise.next_exercise !== 'last' && (
							<button
								type="button"
								onClick={handleSubmit}
								className={`content__ready--button ${exercise.is_completed && 'blocked'}`}
								disabled={exercise.is_completed && true}
							>
								Completar Exercicio
							</button>
						)}
						{exercise.next_exercise === 'last' && (
							<button
								type="button"
								onClick={completeCourse}
								className={`content__ready--button ${exercise.is_completed && 'blocked'}`}
								disabled={exercise.is_completed && true}
							>
								Completar módulo
							</button>
						)}
					</div>
				</section>
			</main>
		</div>
	)
}

export default Exercise
