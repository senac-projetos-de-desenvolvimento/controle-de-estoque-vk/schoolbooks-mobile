import React, { useEffect, useRef, useState } from 'react'
import jwt_decode from 'jwt-decode'
import edit from '../../assets/icons/pencil.svg'
import SiteHeader from '../../components/siteHeader'
import api from '../../services/api'
import './styles.css'
import ProfileCard from '../../components/profileCard'
import CardProfileHistory from '../../components/cardProfileHistory'
import dummy from '../../assets/icons/dummy.png'
import Modal from '../../components/modal'

function Profile() {
	const usernameInput = useRef(null)
	const [avatar, setAvatar] = useState()
	const [update, setUpdate] = useState(0)
	const [loading, setLoading] = useState(false)
	const [tutorials, setTutorials] = useState([])
	const [profile, setProfile] = useState({})
	const [showNameInput, setShowNameInput] = useState(false)
	const [completeName, setCompleteName] = useState('')
	const [biography, setBiography] = useState('')
	const [pictureError, setPictureError] = useState(false)

	async function _handleKeyDown(e) {
		if (e.key === 'Enter') {
			const token_data = jwt_decode(localStorage.getItem('@conig-Token'))
			try {
				await api
					.put(`/profile/${token_data.uid}`, {
						completeName,
						biography,
						informations: true
					})
					.then(response => response.data)
					.then(result => {
						console.log(result)
						setUpdate(update + 2)
						setShowNameInput(false)
					})
			} catch (error) {
				alert(`${error} Ocorreu um erro ao buscar os items`)
			}
		}
	}
	async function _handleBlur(e) {
		const token_data = jwt_decode(localStorage.getItem('@conig-Token'))
		try {
			await api
				.put(`/profile/${token_data.uid}`, {
					completeName,
					biography,
					informations: true
				})
				.then(response => response.data)
				.then(result => {
					console.log(result)
					setUpdate(update + 1)
					setShowNameInput(false)
				})
		} catch (error) {
			alert(`${error} Ocorreu um erro ao buscar os items`)
		}
	}
	useEffect(() => {
		async function getProfile() {
			const token_data = jwt_decode(localStorage.getItem('@conig-Token'))
			try {
				const { data } = await api.get(`/profile`)
				setProfile(data)
				setCompleteName(data.complete_name)
				setBiography(data.biography)
				setTutorials(data.studentTutorial)
				setLoading(true)
			} catch (error) {
				alert(`${error} Ocorreu um erro ao buscar os items`)
			}
		}
		getProfile()
		setLoading(true)
	}, [update])

	async function handleAvatar(e) {
		e.preventDefault()
		const imagefile = document.querySelector('#image-input')
		setAvatar(imagefile.files[0])

		const formData = new FormData()
		formData.append('avatar', imagefile.files[0])
		const token_data = jwt_decode(localStorage.getItem('@conig-Token'))

		fetch(`https://api.conig.info/profile/${token_data.uid}`, {
			method: 'PUT',
			body: formData,
			headers: {
				authorization: `Bearer ${localStorage.getItem('@conig-Token')}`
			}
		})
			.then(response => response.json())
			.then(result => {
				console.log('Success:', result)
				setUpdate(update + 1)
			})
			.catch(error => {
				console.error('Error:', error)
				setPictureError(true)
			})
	}
	async function openInputName() {
		setShowNameInput(true)

		await usernameInput.current.focus()
	}
	function openFile() {
		document.getElementById('image-input').click()
	}
	async function closeModal(event) {
		event.preventDefault()
		setPictureError(false)
	}
	return profile.studentTutorial ? (
		<div>
			<SiteHeader />
			{pictureError && (
				<Modal
					title="Ops... Algo deu errado"
					message="Parece que algo deu errado ao tentar mudar sua foto. Por favor tente novamente, mas tenha em mente, a foto deve ser mais leve que 2MB."
					show={pictureError}
					handleClose={e => closeModal(e)}
				/>
			)}
			<div className="main-container">
				<button onClick={openFile} id="image-edit__button">
					<img
						src={
							profile.profile_pic
								? `https://api.conig.info/uploads/profile_pic/${profile.profile_pic}`
								: dummy
						}
						id="profile-pic"
					/>
					<div id="image-edit">
						<img src={edit} id="image-edit__icon" />
					</div>
				</button>
				<form encType="multipart/form-data">
					<input
						type="file"
						id="image-input"
						onChange={e => handleAvatar(e)}
						accept="image/x-png,image/jpeg"
					/>
				</form>

				<input
					className={`profile-username  ${
						showNameInput === true ? 'profile-none' : 'profile-display'
					}`}
					type="text"
					value={completeName}
					onBlur={_handleBlur}
					onKeyDown={_handleKeyDown}
					onChange={e => setCompleteName(e.target.value)}
				/>

				<span className="profile-username__showname">@{profile.username}</span>
				<span className="profile-username__showname">Nivel {profile.level}</span>
				<hr className="profile-hr" />
				<div className="profile-user-informations">
					<label htmlFor="description">Sobre mim</label>
					<br />
					<textarea
						name="description"
						id="profile-user__description"
						placeholder="Digite sua biografia aqui"
						value={biography}
						onBlur={_handleBlur}
						onKeyDown={_handleKeyDown}
						onChange={e => setBiography(e.target.value)}
					/>
					Minhas conquistas:
					{profile.studentCollectibles.length > 0 &&
						profile.studentCollectibles.map(collectible => (
							<ProfileCard achievement={collectible.collectible.description} />
						))}
				</div>
				<hr className="profile-vertical-hr" />
				<div className="profile-history">
					Tutoriais/Exercicios completados:
					{profile.studentTutorial.length > 0 &&
						profile.studentTutorial.map(tutorial => (
							<CardProfileHistory exercise={false} tutorial={tutorial.tutorial.title} />
						))}
					{profile.studentExercise.length > 0 &&
						profile.studentExercise.map(tutorial => (
							<CardProfileHistory exercise={tutorial.exercise.title} tutorial={false} />
						))}
				</div>
			</div>
		</div>
	) : (
		<></>
	)
}

export default Profile
