import React, { useEffect, useState } from 'react'
import ReactMarkdown from 'react-markdown'
import RankingCard from '../../components/rankingCard'
import SiteHeader from '../../components/siteHeader'
import api from '../../services/api'
import './styles.css'

function Ranking() {
	const [weeklyRanking, setWeeklyRanking] = useState([])
	const [monthRanking, setMonthRanking] = useState([])
	const [generalRanking, setGeneralRanking] = useState([])

	useEffect(() => {
		async function getRankings() {
			const response = await api.get('/rankings')

			setWeeklyRanking(response.data.weekly_ranking)
			setMonthRanking(response.data.month_ranking)
			setGeneralRanking(response.data.general_ranking)
		}

		getRankings()
	}, [])

	return (
		<div>
			<SiteHeader />
			<div className="ranking-container">
				<main className="ranking-main-container">
					<div className="ranking__title">
						<p className="ranking__title-title">
							Olá seja bem vindo ao nosso{' '}
							<span className="ranking__title-detach">RANKING!!!!!</span>
						</p>
						<p className="ranking__title-subtitle">
							Então... Aqui você <span className="ranking__title-detach">INCRIVELMENTE</span> pode
							ver se está no <span className="ranking__title-detach">TOP 10</span> da nossa
							plataforma, sendo durante essa semana, mês, ou de{' '}
							<span className="ranking__title-detach">TODO</span> o período.
						</p>
					</div>
					<RankingCard data={weeklyRanking} title="Ranking Semanal" />
					<RankingCard data={monthRanking} title="Ranking Mensal" />
					<RankingCard data={generalRanking} title="Ranking Geral" />
				</main>
			</div>
		</div>
	)
}

export default Ranking
