import React, { useEffect, useState } from 'react'
import SiteFooter from '../../components/siteFooter'

import SiteHeader from '../../components/siteHeader'
import api from '../../services/api'
import './styles.css'
function InitialPage() {
	return (
		<div>
			<SiteHeader />
			<div className="py-12 bg-white">
				<div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
					<div className="lg:text-center">
						<h2 className="text-base text-indigo-600 font-semibold tracking-wide uppercase">
							Conig - Plataforma de Aprendizagem
						</h2>
						<p className="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
							O melhor jeito de iniciar no mundo da programação
						</p>
						<p className="mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto">
							Perfeito para você que esta iniciando no mundo da programação hoje. E até mesmo para
							quem quer aprimorar suas habilidades.
						</p>
					</div>

					<div className="mt-10">
						<dl className="space-y-10 md:space-y-0 md:grid md:grid-cols-2 md:gap-x-8 md:gap-y-10">
							<div className="flex">
								<div className="flex-shrink-0">
									<div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
										<svg
											className="w-6 h-6"
											fill="none"
											stroke="currentColor"
											viewBox="0 0 24 24"
											xmlns="http://www.w3.org/2000/svg"
										>
											<path
												stroke-linecap="round"
												stroke-linejoin="round"
												stroke-width="2"
												d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253"
											></path>
										</svg>
									</div>
								</div>
								<div className="ml-4">
									<dt className="text-lg leading-6 font-medium text-gray-900">Cursos Gratuitos</dt>
									<dd className="mt-2 text-base text-gray-500">
										Desfrute de cursos introdutórios gratuitos com professores que sabem do que
										estão falando.
									</dd>
								</div>
							</div>

							<div className="flex">
								<div className="flex-shrink-0">
									<div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
										<svg
											className="h-6 w-6"
											xmlns="http://www.w3.org/2000/svg"
											fill="none"
											viewBox="0 0 24 24"
											stroke="currentColor"
											aria-hidden="true"
										>
											<path
												stroke-linecap="round"
												stroke-linejoin="round"
												stroke-width="2"
												d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9"
											/>
										</svg>
									</div>
								</div>
								<div className="ml-4">
									<dt className="text-lg leading-6 font-medium text-gray-900">Aulas Online</dt>
									<dd className="mt-2 text-base text-gray-500">
										Na Conig as aulas são disponibilizadas de maneira totalmente online, com acesso
										vitalicio.
									</dd>
								</div>
							</div>

							<div className="flex">
								<div className="flex-shrink-0">
									<div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
										<svg
											className="w-6 h-6"
											fill="none"
											stroke="currentColor"
											viewBox="0 0 24 24"
											xmlns="http://www.w3.org/2000/svg"
										>
											<path
												stroke-linecap="round"
												stroke-linejoin="round"
												stroke-width="2"
												d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z"
											></path>
										</svg>
									</div>
								</div>
								<div className="ml-4">
									<dt className="text-lg leading-6 font-medium text-gray-900">
										Dúvidas? Contate-nos
									</dt>
									<dd className="mt-2 text-base text-gray-500">
										Na Conig é possivel retirar dúvidas diretamente conosco, mande um email para:
										conigplat@gmail.com.
									</dd>
								</div>
							</div>

							<div className="flex">
								<div className="flex-shrink-0">
									<div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
										<svg
											className="h-6 w-6"
											xmlns="http://www.w3.org/2000/svg"
											fill="none"
											viewBox="0 0 24 24"
											stroke="currentColor"
											aria-hidden="true"
										>
											<path
												stroke-linecap="round"
												stroke-linejoin="round"
												stroke-width="2"
												d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z"
											/>
										</svg>
									</div>
								</div>
								<div className="ml-4">
									<dt className="text-lg leading-6 font-medium text-gray-900">
										Cursos intermediários/avançados
									</dt>
									<dd className="mt-2 text-base text-gray-500">
										Na Conig também disponibilizamos cursos para quem já esta no mundo da
										programação, sendo estes de níveis intermediários até avançados.
									</dd>
								</div>
							</div>
						</dl>
					</div>
				</div>
			</div>
			<SiteFooter></SiteFooter>
		</div>
	)
}

export default InitialPage
