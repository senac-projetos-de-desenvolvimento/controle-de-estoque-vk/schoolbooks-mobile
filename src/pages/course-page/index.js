import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import ReactPlayer from 'react-player'
import logo_conig from '../../assets/images/logo.png'
import SiteHeader from '../../components/siteHeader'
import api from '../../services/api'
import './styles.css'

function Course_Page() {
	const [course, setCourse] = useState([])
	const { id } = useParams()

	useEffect(() => {
		async function getCourse() {
			try {
				const { data } = await api.get(`/courses/${id}`)

				setCourse(data)
			} catch (error) {
				alert(`${error}Um erro ocorreu`)
			}
		}
		getCourse()
	}, [id])
	return (
		<div>
			<SiteHeader />
			{course.map(course => (
				<div>
					<div className="top">
						<img src={course.top_image} alt="Course_image" className="course-image" />
					</div>
					<div className="grid grid-cols-1 sm:grid-cols-1">
						<div>
							<h1 className="text-center title text-indigo-600 mt-4">{course.course_name}</h1>
							<div className="content">
								<div className="text-justify description justify-self-center justify-between bg-gray-200 mt-4">
									<p className=" mt-2 text-center">
										<span className="text-indigo-600 description__title">Descrição</span>
									</p>
									<p className="mt-6 p-2">
										<span className="text-gray-700 description__content">{course.description}</span>
									</p>
								</div>
								<div className="text-justify description justify-self-center justify-between bg-gray-100 mt-4 submodules">
									<p className=" mt-2 text-center modules__title">
										<span className="text-indigo-600">Sub módulos</span>
									</p>
									<div className="mt-6 submodule">
										<div className="submodule__card">
											<span className="submodule__info">Introdução ao javascript</span>
											<span className="submodule__classes">4 aulas</span>
										</div>
										<div className="submodule__card">
											<span className="submodule__info">Introdução ao javascript</span>
											<span className="submodule__classes">4 aulas</span>
										</div>
										<div className="submodule__card">
											<span className="submodule__info">Introdução ao javascript</span>
											<span className="submodule__classes">4 aulas</span>
										</div>
									</div>
									<span className="submodule__warning">
										Todos os conteúdos acima já estão disponíveis!
									</span>
								</div>
								<div className="sell-section">
									<ReactPlayer
										url="https://www.youtube.com/watch?v=ysz5S6PUM-U"
										className="presenting-video"
									/>
									<button type="button" className="view_more">
										Veja melhor
									</button>
								</div>
							</div>

							<section className="comments">
								<h1>Comentários</h1>
								<form className="new-comment">
									<textarea placeholder="Digite seu comentário" />
									<button type="submit">Enviar comentário</button>
								</form>
								<ul>
									<li className="comment-container">
										<div className="person">
											<img src={logo_conig} alt="" />
											<span>João Silva</span>
										</div>
										<span className="person-comment">
											Eu gostei muito do curso porém acredito que ele poderia ser mais didático
										</span>
									</li>
									<li className="comment-container">
										<div className="person">
											<img src={logo_conig} alt="" />
											<span>João Silva</span>
										</div>
										<span className="person-comment">
											Eu gostei muito do curso porém acredito que ele poderia ser mais didático
										</span>
									</li>
								</ul>
							</section>
						</div>
					</div>
				</div>
			))}
		</div>
	)
}

export default Course_Page
