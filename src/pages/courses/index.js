import React, { useEffect, useState } from 'react'
import SiteFooter from '../../components/siteFooter'
import SiteHeader from '../../components/siteHeader'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import Slider from 'react-slick'
import api from '../../services/api'
import './styles.css'

function Courses() {
	const [coursesListDefault, setCoursesListDefault] = useState([])
	const [courses, setCourses] = useState([])
	const [input, setInput] = useState('')
	const settings = {
		className: 'slider variable-width slider',
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1
	}

	useEffect(() => {
		async function getCourses() {
			try {
				const { data } = await api.get('/modules')
				setCourses(data)
				setCoursesListDefault(data)
			} catch (error) {
				alert(`${error} Ocorreu um erro ao buscar os items`)
			}
		}
		getCourses()
	}, [])

	setTimeout(() => {
		window.dispatchEvent(new Event('resize'))
	}, 150)
	console.log(courses)
	const updateInput = async input => {
		const filtered = coursesListDefault.filter(course => {
			return course.course_name.toLowerCase().includes(input.toLowerCase())
		})
		setInput(input)
		setCourses(filtered)
	}

	return (
		<div>
			<SiteHeader />
			<Slider className="carousel_mobile" {...settings}>
				{courses.map(course => (
					<div>
						<img
							src={course.module_image_link}
							alt="carousel--image"
							className="bg-contain  carousel--image"
						/>
					</div>
				))}
			</Slider>
			<div className="grid h-96 place-items-center mt-24 w-2xl course-title">
				Nossos Módulos
				<hr />
				<div className="courses-panel">
					<div>
						<input
							placeholder="Procurar módulo"
							value={input}
							id="search--bar"
							onChange={e => updateInput(e.target.value)}
							className="md:ml-8 w-1/3 text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
						/>
					</div>
					{courses.map(module => (
						<a
							href={`module/${module.id}`}
							className="flex flex-col md:ml-auto h-46 w-2xl shadow-2xl md:mt-2 gap-2 m-5 max-w-5xl m-auto course"
						>
							<div className="bg-white w-full course_card flex items-center p-2 rounded-xl shadow border hover:bg-yellow-300">
								<div className="relative flex items-center space-x-4">
									<img
										src={module.module_image_link}
										alt="My profile"
										className="w-16 h-16 rounded-full course_image_card"
									/>
								</div>
								<div className="flex-grow p-3">
									<div className="font-semibold course_title_text text-gray-700">{module.name}</div>
									<div className="course-description">
										{module.description.length > 70
											? `${module.description.substr(0, 70)}...`
											: module.description}
									</div>
								</div>
								<div className="p-2 svg_container">
									<svg
										className="w-6 h-6 course_card_svg"
										fill="none"
										stroke="currentColor"
										viewBox="0 0 24 24"
										xmlns="http://www.w3.org/2000/svg"
									>
										<path
											strokeLinecap="round"
											strokeLinejoin="round"
											strokeWidth="2"
											d="M13 5l7 7-7 7M5 5l7 7-7 7"
										/>
									</svg>
								</div>
							</div>
						</a>
					))}
				</div>
			</div>
		</div>
	)
}

export default Courses
