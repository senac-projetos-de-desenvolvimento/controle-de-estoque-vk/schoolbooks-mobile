import React from 'react'

const NotificationCard = ({ show, title, content }) => {
	let showHideClassName = 'display-none'
	if (show) {
		showHideClassName = 'display-absolute is-show'
	} else {
		setTimeout(() => {
			showHideClassName = 'display-none is-out'
		}, 5000)
	}
	return (
		<div className={`notification ${showHideClassName}`}>
			<span className="notification--title">{title}</span>
			<hr className="notification--hr" />
			<span className="notification--content">{content}</span>
		</div>
	)
}

export default NotificationCard
