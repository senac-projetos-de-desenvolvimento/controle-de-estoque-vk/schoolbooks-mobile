import React, { useEffect, useState } from 'react'
import api from '../../services/api'
import ReactMarkdown from 'react-markdown'
import jwt_decode from 'jwt-decode'
import './styles.css'
import { Redirect, useHistory } from 'react-router'

const Modal = ({ show, children, next_tutorial, handleClose, tutorial_id }) => {
	const [feedbackType, setFeedbackType] = useState('')
	const [feedbackText, setFeedbackText] = useState('')
	const [showModal, setShowModal] = useState(show)
	const [showHideClassName, setShowHideClassName] = useState('modal display-none')
	let history = useHistory()

	useEffect(() => {
		setShowHideClassName(show ? 'modal display-block' : 'modal display-none')
	}, [show])

	async function handleFeedback(event) {
		event.preventDefault()
		const token_data = jwt_decode(localStorage.getItem('@conig-Token'))

		const response = await api
			.post('/tutorialFeedbacks', {
				feedback_type: feedbackType,
				feedback_text: feedbackText,
				student_id: token_data.uid,
				tutorial_id: tutorial_id
			})
			.then(function(response) {
				console.log(response)
			})
			.catch(function(error) {
				console.log(error)
			})
		setShowHideClassName('modal display-none')
		handleClose()
		// window.location.reload()
	}
	return (
		<div className={showHideClassName}>
			<div className="modal-main">
				<div className="modal-title">Dê o seu feedback.</div>
				<hr className="modal-hr"></hr>
				<div className="modal-content">
					<ReactMarkdown>
						Ola jovem estudante, como nossa plataforma é nova ainda, gostariamos de receber o máximo
						de feedbacks possiveis, se puder fazer a gentileza de nos dar o seu feedback sobre esta
						aula estamos agradecidos, caso contrário clique em fechar e será redirecionado para a
						próxima aula
					</ReactMarkdown>
				</div>
				<form onSubmit={handleFeedback}>
					<div className="form-group">
						<label for="feedback_type" className="modal_label">
							O que gostaria de nos contar sobre?
						</label>
						<select
							name="feedback_type"
							className="modal_input"
							value={feedbackType}
							onChange={e => setFeedbackType(e.target.value)}
						>
							<option value="GOSTEI">Elogio</option>
							<option value="ERRO">Encontrei um erro</option>
							<option value="OUTROS">Outros</option>
						</select>
					</div>
					<div className="form-group">
						<label for="feedback_text" className="modal_label">
							Conte-nos os detalhes:
						</label>
						<input
							type="text"
							name="feedback_text"
							className="modal_input"
							placeholder="Gostei da aula porque..."
							value={feedbackText}
							onChange={e => setFeedbackText(e.target.value)}
						></input>
					</div>
					<div className="modal_buttons">
						<button className="modal-button__send" onClick={handleFeedback}>
							Enviar feedback
						</button>
						<a href={next_tutorial} className="modal-button__close">
							Fechar
						</a>
					</div>
				</form>
			</div>
		</div>
	)
}

export default Modal
