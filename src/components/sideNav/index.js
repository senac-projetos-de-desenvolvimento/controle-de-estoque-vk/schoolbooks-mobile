import React from 'react'
import { Link } from 'react-router-dom'
import './styles.css'
const SideNav = ({ data = [], module_id = 0, module_name, tutorial_id, is_exercise = false }) => {
	const activeClass = is_exercise ? '' : 'active__submodule'
	const exerciseActive = is_exercise ? 'active__submodule' : ''
	console.log(data)
	return (
		<div className="sidenav">
			<nav className="sidenav__nav">
				<h1 className="sidenav__title">{module_name}</h1>
				<span className="sidenav__subtitle">Módulo {module_name}</span>
				<div className="sidenav__menu">
					{data.map(class_ => (
						<div className="sidenav__menu" key={class_.id}>
							<span className="sidenav__submodule__title">{class_.title}</span>
							<ul className="sidenav__submodule__classes">
								{class_.tutorials.map(tutorial => (
									<div key={tutorial.id}>
										{parseInt(tutorial.id) === parseInt(tutorial_id) && (
											<li className={`sidenav__submodule__tutorial ${activeClass}`}>
												<Link
													to={`/module/${module_id}/tutorial/${tutorial.id}`}
													className={`${
														tutorial.studentTutorial.length > 0
															? 'sidenav__submodule__link_viewed'
															: 'sidenav__submodule__link'
													} ${activeClass}`}
												>
													{tutorial.title}
												</Link>
											</li>
										)}
										{parseInt(tutorial.id) !== parseInt(tutorial_id) && (
											<li className="sidenav__submodule__tutorial">
												<Link
													to={`/module/${module_id}/tutorial/${tutorial.id}`}
													className={`${
														tutorial.studentTutorial.length > 0
															? 'sidenav__submodule__link_viewed'
															: 'sidenav__submodule__link'
													}`}
												>
													{tutorial.title}
												</Link>
											</li>
										)}
									</div>
								))}
								{class_.exercises.map(exercise => (
									<div key={exercise.id}>
										{parseInt(exercise.id) === parseInt(tutorial_id) && (
											<li className={`sidenav__submodule__tutorial ${exerciseActive}`}>
												<Link
													to={`/module/${module_id}/exercise/${exercise.id}`}
													className={`${
														exercise.studentExercise.length > 0
															? 'sidenav__submodule__link_viewed'
															: 'sidenav__submodule__link'
													} ${exerciseActive}`}
												>
													{exercise.title}
												</Link>
											</li>
										)}
										{parseInt(exercise.id) !== parseInt(tutorial_id) && (
											<li className="sidenav__submodule__tutorial">
												<Link
													to={`/module/${module_id}/exercise/${exercise.id}`}
													className={`${
														exercise.studentExercise.length > 0
															? 'sidenav__submodule__link_viewed'
															: 'sidenav__submodule__link'
													}`}
												>
													{exercise.title}
												</Link>
											</li>
										)}
									</div>
								))}
							</ul>
						</div>
					))}
				</div>
			</nav>
		</div>
	)
}

export default SideNav
