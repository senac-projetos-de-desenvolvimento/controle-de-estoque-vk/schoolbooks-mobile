import React from 'react'
import ProgressBar from '@ramonak/react-progress-bar'
import dummy from '../../assets/images/dummy.png'

const cls =
	'block px-6 py-2 mobile text-xs font-medium leading-6 text-center text-white uppercase transition bg-purple-600 rounded-full shadow ripple hover:shadow-lg hover:bg-yellow-500 focus:outline-none float-right m-6'
const Button = ({ onClick, name, image, icon, progress, level }) => (
	<button type="button" className={cls} onClick={onClick}>
		{name}
		{icon}
		<img
			src={image ? `https://api.conig.info/uploads/profile_pic/${image}` : dummy}
			alt="alt"
			className="rounded-full mobile_image w-6 h-6 float-right md:ml-2"
		/>
		<div className="flex justify-center items-center">
			<ProgressBar
				width={90}
				margin={5}
				completed={progress}
				height={5}
				bgColor="#d97706"
				isLabelVisible={false}
			/>
			<span>Nv. {level}</span>
		</div>
	</button>
)

export default Button
