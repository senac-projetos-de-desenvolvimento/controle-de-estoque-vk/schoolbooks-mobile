import React from "react";
import "./styles.css";
function Input(props, ...rest) {
  return (
    <div className="input-block">
      <input
        type={props.type}
        placeholder={props.placeholder}
        name={props.name}
        onChange={props.onChange}
        {...rest}
      />
    </div>
  );
}

export default Input;
