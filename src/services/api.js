import axios from 'axios'
import useSWR from 'swr'
import { getToken } from './auth'

// const api = axios.create({
// 	baseURL: 'http://127.0.0.1:3333'
// })
const api = axios.create({
	baseURL: 'https://api.conig.info/'
})

export function useFetch(url_api) {
	const { data, error, mutate } = useSWR(
		url_api,
		async url => {
			const response = await api.get(url)

			return response.data
		},
		{ refreshInterval: 1000, refreshInterval: 1 }
	)

	return { data, error, mutate }
}

api.interceptors.request.use(async config => {
	const token = getToken()
	if (token) {
		config.headers.Authorization = `Bearer ${token}`
	}
	return config
})

export default api
